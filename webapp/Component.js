sap.ui.define(
	[
		"sap/ui/core/Component",
		"sap/ui/Device",
		"be/fiddle/snowflakes/control/Flake"
	],
	function(BaseComponent, Device, Flake) {
		"use strict";

		/**
		 * @name        be.fiddle.snowflakes.Component
		 * @alias       Component
		 * @instance
		 * @public
		 * @class
		 * @todo please replace Component with a meaningfulname.
		 * <p></p>
		 */
		const Component = BaseComponent.extend(
			"be.fiddle.snowflakes.Component",
			/**@lends be.fiddle.snowflakes.Component.prototype **/ {
				metadata: {
					manifest: "json"
				}
			}
		);

		/**
		 * @method init
		 * @public
		 * @instance
		 * @memberof be.fiddle.snowflakes.Component
		 * <p> </p>
		 * 
		 */
		Component.prototype.init = function(){
			if (BaseComponent.prototype.init ) {
				BaseComponent.prototype.init.apply(this,arguments);
			}

			// define snow variables
			this._flakes = [];

			//in case there's no canvas yet.
			try {
				//this old jquery syntax no longer works
				//$("#shell-cntnt").prepend('<canvas id="snowflakes" height="600" width="800" aria-hidden="true" style="position:absolute;z-index:-1"></canvas>');

				this._canvas = document.createElement("canvas");
				this._canvas.setAttribute("height", "600px");
				this._canvas.setAttribute("width", "800px");
				this._canvas.setAttribute("aria-hidden", true);
				this._canvas.setAttribute("style", "position:absolute;z-index:-1");
				this._canvas.id = "snowflakes";
	
				document.getElementById( "backgroundImage-shellArea" ).appendChild( this._canvas )
			} catch (e){
				//failure
			}

			// get canvas to paint in: there seems to be only one in the Fiori Launchpad so this should always work
			this._canvas = document.getElementById("snowflakes"); 
			if (!this._canvas) return; 

			window.addEventListener("resize", this.onResize.bind(this) );
			this.onResize();

			// first loop to create all Flake objects
			let i = 200;
			while (i--) {
				this._flakes.push(new Flake());
			}

			// start looping all flakes to move them
			requestAnimationFrame( this.render.bind(this) );
		};	

		/**
		 * @method destroy
		 * @public
		 * @instance
		 * @memberof be.fiddle.snowflakes.Component
		 * <p> </p>
		 * 
		 */
		Component.prototype.destroy = function(){
			this._flakes.forEach( flake => flake.destroy() );
			delete this._flakes;
			window.removeEventListener("resize", this.onResize.bind(this) );
			document.getElementById("backgroundImage-shellArea").removeChild( this._canvas );

			BaseComponent.prototype.destroy.apply(this,arguments);
		};

		/**
		 * @method onResize
		 * @public
		 * @instance
		 * @memberof be.fiddle.snowflakes.Component
		 * <p> </p>
		 * 
		 */
		Component.prototype.onResize = function(){
			this._canvas.width = window.innerWidth;
			this._canvas.height = window.innerHeight;
		};

		/**
		 * @method loop
		 * @public
		 * @instance
		 * @memberof be.fiddle.snowflakes.Component
		 * <p></p>
		 */
		Component.prototype.render = function() { 
			if (!this._flakes ) return; //no more flakes to draw
			
			// clear canvas
			let context = this._canvas.getContext("2d");
			context.clearRect(0, 0, window.innerWidth, window.innerHeight);
			
			// loop through the flakes and "animate" them
			let i = this._flakes.length;
			while (i--) {
				this._flakes[i].update(context);					
			}

			// continue animation...
			requestAnimationFrame( this.render.bind(this) );
		};

		return Component;
	}
);
